.PHONY: all generate generate-with-docker clean build test

default_openapi_jar_path = openapi-generator-cli.jar
ifeq "$(OPENAPI_GENERATOR_JAR_PATH)" ""
	openapi_jar_path := $(default_openapi_jar_path)
else
	openapi_jar_path := $(OPENAPI_GENERATOR_JAR_PATH)
endif

default_client_gen_path = v1/pkg/client
ifeq "$(CLIENT_GEN_PATH)" ""
	client_gen_path := $(default_client_gen_path)
else
	client_gen_path := $(CLIENT_GEN_PATH)
endif

all: clean generate build test

## generate-common: common function used by generate by docker and java jar
generate-common:
	rm -rf $(client_gen_path)/*.sh \
		$(client_gen_path)/api \
		$(client_gen_path)/.openapi-generator*

	mv $(client_gen_path)/docs .
	mv $(client_gen_path)/go.mod .
	mv $(client_gen_path)/go.sum .
	go mod tidy -v # This is needed to fix go.sum
	go fmt ./...

## generate-with-docker: Generate the sdk files using the docker runtime
generate-with-docker:
	@echo "Generating code with docker..."
	docker run --rm -v "$$(pwd):/local" \
		--user $(shell id -u):$(shell id -g) \
		openapitools/openapi-generator-cli:v5.2.0 generate \
		-i /local/spec/openapi-v1.yaml \
		-c /local/open-api-conf.yaml \
		-g go \
		-o /local/$(client_gen_path) \
		-t /local/templates
	
	$(MAKE) generate-common

## generate: Generate the sdk files using the java runtime
generate:
	@echo "Generating code..."
	java -jar $(openapi_jar_path) generate \
		-i spec/openapi-v1.yaml \
		-c open-api-conf.yaml \
		-t templates \
		-g go -o $(client_gen_path)
	rm $(openapi_jar_path)

	$(MAKE) generate-common

clean:
	@echo "Cleaning up..."
	rm -rf docs
	rm -f $(client_gen_path)/*.go go.mod go.sum

build:
	go build ./...

build-examples:
	cd examples/current-block && go mod tidy -v && go build && go clean && cd ../..
	cd examples/platforms-overview && go mod tidy -v && go build && go clean && cd ../..
	cd examples/tx-pagination && go mod tidy -v && go build && go clean && cd ../..
	cd examples/tx-querying && go mod tidy -v && go build && go clean && cd ../..
	cd examples/tx-send && go mod tidy -v && go build && go clean && cd ../..
	cd examples/ws-blocks && go mod tidy -v && go build && go clean && cd ../..
	cd examples/address-balances && go mod tidy -v && go build && go clean && cd ../..

test:
	go test ./...

build-mocks:
	mockery --dir ../client --name TransactionsAPI --outpkg mocks --output mocks --case snake