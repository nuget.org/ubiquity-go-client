package main

import (
	"context"
	"fmt"
	"os"
	"strings"

	"gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/examples"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

/**
Getting current block and iterating over transactions.

Env variables:
	1) UBI_ACCESS_TOKEN - required, Ubiquity API Access Token
	2) UBI_ENDPOINT - optional, Ubiquity API custom endpoint (prod by default)
	3) UBI_PLATFORM - optional, platform e.g. ethereum (bitcoin by default)
*/
func main() {
	// Access token is required
	accessToken := os.Getenv("UBI_ACCESS_TOKEN")
	if accessToken == "" {
		panic(fmt.Errorf("env variable 'UBI_ACCESS_TOKEN' must be set"))
	}

	// You can *optionally* set a custom endpoint or it will use prod
	config := ubiquity.NewConfiguration()
	if endpoint := os.Getenv("UBI_ENDPOINT"); endpoint != "" {
		config.Servers = ubiquity.ServerConfigurations{
			{
				URL:         endpoint,
				Description: "Custom endpoint",
			},
		}
	}

	// Creating client
	apiClient := ubiquity.NewAPIClient(config)

	// Context and platform
	ctx := context.WithValue(context.Background(), ubiquity.ContextAccessToken, accessToken)

	// Use ubiquity.PlatformsAPI.GetPlatforms(ctx _context.Context) to fetch all supported platforms
	// See examples/platforms-overview
	var pl string
	if pl = os.Getenv("UBI_PLATFORM"); pl == "" {
		pl = "bitcoin"
	}

	// Getting a current block
	block, resp, err := apiClient.BlocksAPI.GetBlock(ctx, pl, "mainnet", "current").Execute()
	if err != nil {
		panic(fmt.Errorf("failed to get a current %s block: got status '%s' and error '%v'",
			strings.Title(pl), resp.Status, err))
	}
	fmt.Printf("Current %s block comes with ID '%s' and transactions count %d\n",
		strings.Title(pl), block.GetId(), len(block.GetTxs()))

	if len(block.GetTxs()) > 0 {
		// Printing several transactions
		fmt.Println("Printing transactions from 0 to 10:")
		fmt.Println()
		for i := 0; i < 10; i++ {
			tx := block.GetTxs()[i]

			examples.PrintTxWithEvents(tx)
		}
	}
}
