module gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client

go 1.16

require (
	github.com/btcsuite/btcd v0.22.0-beta
	github.com/btcsuite/btcutil v1.0.3-0.20201208143702-a53e38424cce
	github.com/ethereum/go-ethereum v1.10.16
	github.com/gorilla/websocket v1.4.2
	github.com/stretchr/testify v1.7.0
	golang.org/x/oauth2 v0.0.0-20210323180902-22b0adad7558
)
