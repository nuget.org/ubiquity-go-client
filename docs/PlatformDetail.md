# PlatformDetail

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Source** | Pointer to **string** | Backend API Type | [optional] 
**Handle** | Pointer to **string** |  | [optional] 
**GenesisNumber** | Pointer to **int64** |  | [optional] 
**Endpoints** | Pointer to **[]string** |  | [optional] 

## Methods

### NewPlatformDetail

`func NewPlatformDetail() *PlatformDetail`

NewPlatformDetail instantiates a new PlatformDetail object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewPlatformDetailWithDefaults

`func NewPlatformDetailWithDefaults() *PlatformDetail`

NewPlatformDetailWithDefaults instantiates a new PlatformDetail object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetSource

`func (o *PlatformDetail) GetSource() string`

GetSource returns the Source field if non-nil, zero value otherwise.

### GetSourceOk

`func (o *PlatformDetail) GetSourceOk() (*string, bool)`

GetSourceOk returns a tuple with the Source field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetSource

`func (o *PlatformDetail) SetSource(v string)`

SetSource sets Source field to given value.

### HasSource

`func (o *PlatformDetail) HasSource() bool`

HasSource returns a boolean if a field has been set.

### GetHandle

`func (o *PlatformDetail) GetHandle() string`

GetHandle returns the Handle field if non-nil, zero value otherwise.

### GetHandleOk

`func (o *PlatformDetail) GetHandleOk() (*string, bool)`

GetHandleOk returns a tuple with the Handle field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetHandle

`func (o *PlatformDetail) SetHandle(v string)`

SetHandle sets Handle field to given value.

### HasHandle

`func (o *PlatformDetail) HasHandle() bool`

HasHandle returns a boolean if a field has been set.

### GetGenesisNumber

`func (o *PlatformDetail) GetGenesisNumber() int64`

GetGenesisNumber returns the GenesisNumber field if non-nil, zero value otherwise.

### GetGenesisNumberOk

`func (o *PlatformDetail) GetGenesisNumberOk() (*int64, bool)`

GetGenesisNumberOk returns a tuple with the GenesisNumber field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetGenesisNumber

`func (o *PlatformDetail) SetGenesisNumber(v int64)`

SetGenesisNumber sets GenesisNumber field to given value.

### HasGenesisNumber

`func (o *PlatformDetail) HasGenesisNumber() bool`

HasGenesisNumber returns a boolean if a field has been set.

### GetEndpoints

`func (o *PlatformDetail) GetEndpoints() []string`

GetEndpoints returns the Endpoints field if non-nil, zero value otherwise.

### GetEndpointsOk

`func (o *PlatformDetail) GetEndpointsOk() (*[]string, bool)`

GetEndpointsOk returns a tuple with the Endpoints field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEndpoints

`func (o *PlatformDetail) SetEndpoints(v []string)`

SetEndpoints sets Endpoints field to given value.

### HasEndpoints

`func (o *PlatformDetail) HasEndpoints() bool`

HasEndpoints returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


