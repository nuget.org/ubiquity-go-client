# NFTEvent

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**ContractAddress** | Pointer to **string** |  | [optional] 
**TokenId** | Pointer to **int64** |  | [optional] 
**EventType** | Pointer to **string** |  | [optional] 
**Timestamp** | Pointer to **int64** |  | [optional] 
**FromAccount** | Pointer to **string** |  | [optional] 
**ToAccount** | Pointer to **string** |  | [optional] 
**Transaction** | Pointer to **map[string]interface{}** |  | [optional] 

## Methods

### NewNFTEvent

`func NewNFTEvent() *NFTEvent`

NewNFTEvent instantiates a new NFTEvent object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewNFTEventWithDefaults

`func NewNFTEventWithDefaults() *NFTEvent`

NewNFTEventWithDefaults instantiates a new NFTEvent object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetContractAddress

`func (o *NFTEvent) GetContractAddress() string`

GetContractAddress returns the ContractAddress field if non-nil, zero value otherwise.

### GetContractAddressOk

`func (o *NFTEvent) GetContractAddressOk() (*string, bool)`

GetContractAddressOk returns a tuple with the ContractAddress field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContractAddress

`func (o *NFTEvent) SetContractAddress(v string)`

SetContractAddress sets ContractAddress field to given value.

### HasContractAddress

`func (o *NFTEvent) HasContractAddress() bool`

HasContractAddress returns a boolean if a field has been set.

### GetTokenId

`func (o *NFTEvent) GetTokenId() int64`

GetTokenId returns the TokenId field if non-nil, zero value otherwise.

### GetTokenIdOk

`func (o *NFTEvent) GetTokenIdOk() (*int64, bool)`

GetTokenIdOk returns a tuple with the TokenId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTokenId

`func (o *NFTEvent) SetTokenId(v int64)`

SetTokenId sets TokenId field to given value.

### HasTokenId

`func (o *NFTEvent) HasTokenId() bool`

HasTokenId returns a boolean if a field has been set.

### GetEventType

`func (o *NFTEvent) GetEventType() string`

GetEventType returns the EventType field if non-nil, zero value otherwise.

### GetEventTypeOk

`func (o *NFTEvent) GetEventTypeOk() (*string, bool)`

GetEventTypeOk returns a tuple with the EventType field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetEventType

`func (o *NFTEvent) SetEventType(v string)`

SetEventType sets EventType field to given value.

### HasEventType

`func (o *NFTEvent) HasEventType() bool`

HasEventType returns a boolean if a field has been set.

### GetTimestamp

`func (o *NFTEvent) GetTimestamp() int64`

GetTimestamp returns the Timestamp field if non-nil, zero value otherwise.

### GetTimestampOk

`func (o *NFTEvent) GetTimestampOk() (*int64, bool)`

GetTimestampOk returns a tuple with the Timestamp field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTimestamp

`func (o *NFTEvent) SetTimestamp(v int64)`

SetTimestamp sets Timestamp field to given value.

### HasTimestamp

`func (o *NFTEvent) HasTimestamp() bool`

HasTimestamp returns a boolean if a field has been set.

### GetFromAccount

`func (o *NFTEvent) GetFromAccount() string`

GetFromAccount returns the FromAccount field if non-nil, zero value otherwise.

### GetFromAccountOk

`func (o *NFTEvent) GetFromAccountOk() (*string, bool)`

GetFromAccountOk returns a tuple with the FromAccount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetFromAccount

`func (o *NFTEvent) SetFromAccount(v string)`

SetFromAccount sets FromAccount field to given value.

### HasFromAccount

`func (o *NFTEvent) HasFromAccount() bool`

HasFromAccount returns a boolean if a field has been set.

### GetToAccount

`func (o *NFTEvent) GetToAccount() string`

GetToAccount returns the ToAccount field if non-nil, zero value otherwise.

### GetToAccountOk

`func (o *NFTEvent) GetToAccountOk() (*string, bool)`

GetToAccountOk returns a tuple with the ToAccount field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetToAccount

`func (o *NFTEvent) SetToAccount(v string)`

SetToAccount sets ToAccount field to given value.

### HasToAccount

`func (o *NFTEvent) HasToAccount() bool`

HasToAccount returns a boolean if a field has been set.

### GetTransaction

`func (o *NFTEvent) GetTransaction() map[string]interface{}`

GetTransaction returns the Transaction field if non-nil, zero value otherwise.

### GetTransactionOk

`func (o *NFTEvent) GetTransactionOk() (*map[string]interface{}, bool)`

GetTransactionOk returns a tuple with the Transaction field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTransaction

`func (o *NFTEvent) SetTransaction(v map[string]interface{})`

SetTransaction sets Transaction field to given value.

### HasTransaction

`func (o *NFTEvent) HasTransaction() bool`

HasTransaction returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


