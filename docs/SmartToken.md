# SmartToken

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Type** | **string** | Name of token mechanism (smart contract) | 
**Contract** | **string** | Address of contract | 

## Methods

### NewSmartToken

`func NewSmartToken(type_ string, contract string, ) *SmartToken`

NewSmartToken instantiates a new SmartToken object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewSmartTokenWithDefaults

`func NewSmartTokenWithDefaults() *SmartToken`

NewSmartTokenWithDefaults instantiates a new SmartToken object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetType

`func (o *SmartToken) GetType() string`

GetType returns the Type field if non-nil, zero value otherwise.

### GetTypeOk

`func (o *SmartToken) GetTypeOk() (*string, bool)`

GetTypeOk returns a tuple with the Type field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetType

`func (o *SmartToken) SetType(v string)`

SetType sets Type field to given value.


### GetContract

`func (o *SmartToken) GetContract() string`

GetContract returns the Contract field if non-nil, zero value otherwise.

### GetContractOk

`func (o *SmartToken) GetContractOk() (*string, bool)`

GetContractOk returns a tuple with the Contract field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContract

`func (o *SmartToken) SetContract(v string)`

SetContract sets Contract field to given value.



[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


