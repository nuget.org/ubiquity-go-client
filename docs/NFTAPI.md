# \NFTAPI

All URIs are relative to *https://ubiquity.api.blockdaemon.com/v1*

Method | HTTP request | Description
------------- | ------------- | -------------
[**ExplorerGetCollection**](NFTAPI.md#ExplorerGetCollection) | **Get** /nft/{protocol}/{network}/collection/{id} | 
[**ExplorerListAssets**](NFTAPI.md#ExplorerListAssets) | **Get** /nft/{protocol}/{network}/assets | 
[**ExplorerListCollections**](NFTAPI.md#ExplorerListCollections) | **Get** /nft/{protocol}/{network}/collections | 
[**ExplorerListEvents**](NFTAPI.md#ExplorerListEvents) | **Get** /nft/{protocol}/{network}/events | 



## ExplorerGetCollection

> GetCollectionResponse ExplorerGetCollection(ctx, protocol, network, id).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "protocol_example" // string | Coin platform handle
    network := "network_example" // string | Which network to target
    id := "id_example" // string | Mapped to URL query parameter 'uuid'

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.NFTAPI.ExplorerGetCollection(context.Background(), protocol, network, id).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `NFTAPI.ExplorerGetCollection``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ExplorerGetCollection`: GetCollectionResponse
    fmt.Fprintf(os.Stdout, "Response from `NFTAPI.ExplorerGetCollection`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Coin platform handle | 
**network** | **string** | Which network to target | 
**id** | **string** | Mapped to URL query parameter &#39;uuid&#39; | 

### Other Parameters

Other parameters are passed through a pointer to a apiExplorerGetCollectionRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------




### Return type

[**GetCollectionResponse**](GetCollectionResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ExplorerListAssets

> ListAssetsResponse ExplorerListAssets(ctx, protocol, network).WalletAddress(walletAddress).ContractAddress(contractAddress).TokenIdValue(tokenIdValue).CollectionName(collectionName).SortBy(sortBy).Order(order).PageSize(pageSize).PageToken(pageToken).Attributes(attributes).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "protocol_example" // string | Coin platform handle
    network := "network_example" // string | Which network to target
    walletAddress := "walletAddress_example" // string | Mapped to URL query parameter `wallet_address` (optional)
    contractAddress := "contractAddress_example" // string | Mapped to URL query parameter `contract_address` (optional)
    tokenIdValue := int64(789) // int64 | The int64 value. (optional)
    collectionName := "collectionName_example" // string | Mapped to URL query parameter `collection_name` (optional)
    sortBy := "sortBy_example" // string | One of: name, token_id, mint_date (optional)
    order := "order_example" // string | Mapped to URL query parameter `order` One of: asc, desc (optional)
    pageSize := int32(56) // int32 | Mapped to URL query parameter `page_size` (optional)
    pageToken := "pageToken_example" // string | Mapped to URL query parameter `page_token` base64 encoded cursor (optional)
    attributes := []string{"Inner_example"} // []string | Mapped to URL query parameter `attributes` (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.NFTAPI.ExplorerListAssets(context.Background(), protocol, network).WalletAddress(walletAddress).ContractAddress(contractAddress).TokenIdValue(tokenIdValue).CollectionName(collectionName).SortBy(sortBy).Order(order).PageSize(pageSize).PageToken(pageToken).Attributes(attributes).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `NFTAPI.ExplorerListAssets``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ExplorerListAssets`: ListAssetsResponse
    fmt.Fprintf(os.Stdout, "Response from `NFTAPI.ExplorerListAssets`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Coin platform handle | 
**network** | **string** | Which network to target | 

### Other Parameters

Other parameters are passed through a pointer to a apiExplorerListAssetsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **walletAddress** | **string** | Mapped to URL query parameter &#x60;wallet_address&#x60; | 
 **contractAddress** | **string** | Mapped to URL query parameter &#x60;contract_address&#x60; | 
 **tokenIdValue** | **int64** | The int64 value. | 
 **collectionName** | **string** | Mapped to URL query parameter &#x60;collection_name&#x60; | 
 **sortBy** | **string** | One of: name, token_id, mint_date | 
 **order** | **string** | Mapped to URL query parameter &#x60;order&#x60; One of: asc, desc | 
 **pageSize** | **int32** | Mapped to URL query parameter &#x60;page_size&#x60; | 
 **pageToken** | **string** | Mapped to URL query parameter &#x60;page_token&#x60; base64 encoded cursor | 
 **attributes** | **[]string** | Mapped to URL query parameter &#x60;attributes&#x60; | 

### Return type

[**ListAssetsResponse**](ListAssetsResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ExplorerListCollections

> ListCollectionResponse ExplorerListCollections(ctx, protocol, network).ContractAddress(contractAddress).CollectionName(collectionName).SortBy(sortBy).Order(order).PageSize(pageSize).PageToken(pageToken).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "protocol_example" // string | Coin platform handle
    network := "network_example" // string | Which network to target
    contractAddress := []string{"Inner_example"} // []string | Mapped to URL query parameter 'contract_address' (optional)
    collectionName := []string{"Inner_example"} // []string | Mapped to URL query parameter 'collection_name' (optional)
    sortBy := "sortBy_example" // string | Sort by one of: name (optional)
    order := "order_example" // string | Mapped to URL query parameter `order` One of: asc, desc (optional)
    pageSize := int32(56) // int32 | Mapped to URL query parameter `page_size` (optional)
    pageToken := "pageToken_example" // string | Mapped to URL query parameter `page_token` base64 encoded cursor (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.NFTAPI.ExplorerListCollections(context.Background(), protocol, network).ContractAddress(contractAddress).CollectionName(collectionName).SortBy(sortBy).Order(order).PageSize(pageSize).PageToken(pageToken).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `NFTAPI.ExplorerListCollections``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ExplorerListCollections`: ListCollectionResponse
    fmt.Fprintf(os.Stdout, "Response from `NFTAPI.ExplorerListCollections`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Coin platform handle | 
**network** | **string** | Which network to target | 

### Other Parameters

Other parameters are passed through a pointer to a apiExplorerListCollectionsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **contractAddress** | **[]string** | Mapped to URL query parameter &#39;contract_address&#39; | 
 **collectionName** | **[]string** | Mapped to URL query parameter &#39;collection_name&#39; | 
 **sortBy** | **string** | Sort by one of: name | 
 **order** | **string** | Mapped to URL query parameter &#x60;order&#x60; One of: asc, desc | 
 **pageSize** | **int32** | Mapped to URL query parameter &#x60;page_size&#x60; | 
 **pageToken** | **string** | Mapped to URL query parameter &#x60;page_token&#x60; base64 encoded cursor | 

### Return type

[**ListCollectionResponse**](ListCollectionResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)


## ExplorerListEvents

> ListEventResponse ExplorerListEvents(ctx, protocol, network).ContractAddress(contractAddress).WalletAddress(walletAddress).TokenId(tokenId).EventType(eventType).SortBy(sortBy).Order(order).PageSize(pageSize).PageToken(pageToken).Execute()



### Example

```go
package main

import (
    "context"
    "fmt"
    "os"
    openapiclient "./openapi"
)

func main() {
    protocol := "protocol_example" // string | Coin platform handle
    network := "network_example" // string | Which network to target
    contractAddress := "contractAddress_example" // string | mapped to URL query parameter 'contract_address' (optional)
    walletAddress := "walletAddress_example" // string | mapped to URL query parameter 'wallet_address' (optional)
    tokenId := int64(789) // int64 | mapped to URL query parameter 'token_id' (optional)
    eventType := "eventType_example" // string | mapped to URL query parameter 'event_type' (optional)
    sortBy := "sortBy_example" // string | Sort by one of: timestamp (optional)
    order := "order_example" // string | Mapped to URL query parameter `order` One of: asc, desc (optional)
    pageSize := int32(56) // int32 | Mapped to URL query parameter `page_size` (optional)
    pageToken := "pageToken_example" // string | Mapped to URL query parameter `page_token` base64 encoded cursor (optional)

    configuration := openapiclient.NewConfiguration()
    api_client := openapiclient.NewAPIClient(configuration)
    resp, r, err := api_client.NFTAPI.ExplorerListEvents(context.Background(), protocol, network).ContractAddress(contractAddress).WalletAddress(walletAddress).TokenId(tokenId).EventType(eventType).SortBy(sortBy).Order(order).PageSize(pageSize).PageToken(pageToken).Execute()
    if err != nil {
        fmt.Fprintf(os.Stderr, "Error when calling `NFTAPI.ExplorerListEvents``: %v\n", err)
        fmt.Fprintf(os.Stderr, "Full HTTP response: %v\n", r)
    }
    // response from `ExplorerListEvents`: ListEventResponse
    fmt.Fprintf(os.Stdout, "Response from `NFTAPI.ExplorerListEvents`: %v\n", resp)
}
```

### Path Parameters


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------
**ctx** | **context.Context** | context for authentication, logging, cancellation, deadlines, tracing, etc.
**protocol** | **string** | Coin platform handle | 
**network** | **string** | Which network to target | 

### Other Parameters

Other parameters are passed through a pointer to a apiExplorerListEventsRequest struct via the builder pattern


Name | Type | Description  | Notes
------------- | ------------- | ------------- | -------------


 **contractAddress** | **string** | mapped to URL query parameter &#39;contract_address&#39; | 
 **walletAddress** | **string** | mapped to URL query parameter &#39;wallet_address&#39; | 
 **tokenId** | **int64** | mapped to URL query parameter &#39;token_id&#39; | 
 **eventType** | **string** | mapped to URL query parameter &#39;event_type&#39; | 
 **sortBy** | **string** | Sort by one of: timestamp | 
 **order** | **string** | Mapped to URL query parameter &#x60;order&#x60; One of: asc, desc | 
 **pageSize** | **int32** | Mapped to URL query parameter &#x60;page_size&#x60; | 
 **pageToken** | **string** | Mapped to URL query parameter &#x60;page_token&#x60; base64 encoded cursor | 

### Return type

[**ListEventResponse**](ListEventResponse.md)

### Authorization

[bearerAuth](../README.md#bearerAuth)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints)
[[Back to Model list]](../README.md#documentation-for-models)
[[Back to README]](../README.md)

