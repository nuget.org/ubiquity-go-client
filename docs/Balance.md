# Balance

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Currency** | Pointer to [**Currency**](Currency.md) |  | [optional] 
**ConfirmedBalance** | Pointer to **string** |  | [optional] 
**PendingBalance** | Pointer to **string** |  | [optional] 
**ConfirmedNonce** | Pointer to **int32** |  | [optional] 
**ConfirmedBlock** | Pointer to **int32** |  | [optional] 

## Methods

### NewBalance

`func NewBalance() *Balance`

NewBalance instantiates a new Balance object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewBalanceWithDefaults

`func NewBalanceWithDefaults() *Balance`

NewBalanceWithDefaults instantiates a new Balance object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetCurrency

`func (o *Balance) GetCurrency() Currency`

GetCurrency returns the Currency field if non-nil, zero value otherwise.

### GetCurrencyOk

`func (o *Balance) GetCurrencyOk() (*Currency, bool)`

GetCurrencyOk returns a tuple with the Currency field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetCurrency

`func (o *Balance) SetCurrency(v Currency)`

SetCurrency sets Currency field to given value.

### HasCurrency

`func (o *Balance) HasCurrency() bool`

HasCurrency returns a boolean if a field has been set.

### GetConfirmedBalance

`func (o *Balance) GetConfirmedBalance() string`

GetConfirmedBalance returns the ConfirmedBalance field if non-nil, zero value otherwise.

### GetConfirmedBalanceOk

`func (o *Balance) GetConfirmedBalanceOk() (*string, bool)`

GetConfirmedBalanceOk returns a tuple with the ConfirmedBalance field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConfirmedBalance

`func (o *Balance) SetConfirmedBalance(v string)`

SetConfirmedBalance sets ConfirmedBalance field to given value.

### HasConfirmedBalance

`func (o *Balance) HasConfirmedBalance() bool`

HasConfirmedBalance returns a boolean if a field has been set.

### GetPendingBalance

`func (o *Balance) GetPendingBalance() string`

GetPendingBalance returns the PendingBalance field if non-nil, zero value otherwise.

### GetPendingBalanceOk

`func (o *Balance) GetPendingBalanceOk() (*string, bool)`

GetPendingBalanceOk returns a tuple with the PendingBalance field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetPendingBalance

`func (o *Balance) SetPendingBalance(v string)`

SetPendingBalance sets PendingBalance field to given value.

### HasPendingBalance

`func (o *Balance) HasPendingBalance() bool`

HasPendingBalance returns a boolean if a field has been set.

### GetConfirmedNonce

`func (o *Balance) GetConfirmedNonce() int32`

GetConfirmedNonce returns the ConfirmedNonce field if non-nil, zero value otherwise.

### GetConfirmedNonceOk

`func (o *Balance) GetConfirmedNonceOk() (*int32, bool)`

GetConfirmedNonceOk returns a tuple with the ConfirmedNonce field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConfirmedNonce

`func (o *Balance) SetConfirmedNonce(v int32)`

SetConfirmedNonce sets ConfirmedNonce field to given value.

### HasConfirmedNonce

`func (o *Balance) HasConfirmedNonce() bool`

HasConfirmedNonce returns a boolean if a field has been set.

### GetConfirmedBlock

`func (o *Balance) GetConfirmedBlock() int32`

GetConfirmedBlock returns the ConfirmedBlock field if non-nil, zero value otherwise.

### GetConfirmedBlockOk

`func (o *Balance) GetConfirmedBlockOk() (*int32, bool)`

GetConfirmedBlockOk returns a tuple with the ConfirmedBlock field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetConfirmedBlock

`func (o *Balance) SetConfirmedBlock(v int32)`

SetConfirmedBlock sets ConfirmedBlock field to given value.

### HasConfirmedBlock

`func (o *Balance) HasConfirmedBlock() bool`

HasConfirmedBlock returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


