# Asset

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**TokenId** | Pointer to **int64** |  | [optional] 
**ImageUrl** | Pointer to **string** |  | [optional] 
**Name** | Pointer to **string** |  | [optional] 
**Contract** | Pointer to [**Contract**](Contract.md) |  | [optional] 
**Wallets** | Pointer to [**[]AssetWallet**](AssetWallet.md) |  | [optional] 
**Attributes** | Pointer to [**[]AssetTrait**](AssetTrait.md) |  | [optional] 
**MintDate** | Pointer to **int64** |  | [optional] 

## Methods

### NewAsset

`func NewAsset() *Asset`

NewAsset instantiates a new Asset object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewAssetWithDefaults

`func NewAssetWithDefaults() *Asset`

NewAssetWithDefaults instantiates a new Asset object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTokenId

`func (o *Asset) GetTokenId() int64`

GetTokenId returns the TokenId field if non-nil, zero value otherwise.

### GetTokenIdOk

`func (o *Asset) GetTokenIdOk() (*int64, bool)`

GetTokenIdOk returns a tuple with the TokenId field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTokenId

`func (o *Asset) SetTokenId(v int64)`

SetTokenId sets TokenId field to given value.

### HasTokenId

`func (o *Asset) HasTokenId() bool`

HasTokenId returns a boolean if a field has been set.

### GetImageUrl

`func (o *Asset) GetImageUrl() string`

GetImageUrl returns the ImageUrl field if non-nil, zero value otherwise.

### GetImageUrlOk

`func (o *Asset) GetImageUrlOk() (*string, bool)`

GetImageUrlOk returns a tuple with the ImageUrl field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetImageUrl

`func (o *Asset) SetImageUrl(v string)`

SetImageUrl sets ImageUrl field to given value.

### HasImageUrl

`func (o *Asset) HasImageUrl() bool`

HasImageUrl returns a boolean if a field has been set.

### GetName

`func (o *Asset) GetName() string`

GetName returns the Name field if non-nil, zero value otherwise.

### GetNameOk

`func (o *Asset) GetNameOk() (*string, bool)`

GetNameOk returns a tuple with the Name field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetName

`func (o *Asset) SetName(v string)`

SetName sets Name field to given value.

### HasName

`func (o *Asset) HasName() bool`

HasName returns a boolean if a field has been set.

### GetContract

`func (o *Asset) GetContract() Contract`

GetContract returns the Contract field if non-nil, zero value otherwise.

### GetContractOk

`func (o *Asset) GetContractOk() (*Contract, bool)`

GetContractOk returns a tuple with the Contract field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContract

`func (o *Asset) SetContract(v Contract)`

SetContract sets Contract field to given value.

### HasContract

`func (o *Asset) HasContract() bool`

HasContract returns a boolean if a field has been set.

### GetWallets

`func (o *Asset) GetWallets() []AssetWallet`

GetWallets returns the Wallets field if non-nil, zero value otherwise.

### GetWalletsOk

`func (o *Asset) GetWalletsOk() (*[]AssetWallet, bool)`

GetWalletsOk returns a tuple with the Wallets field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetWallets

`func (o *Asset) SetWallets(v []AssetWallet)`

SetWallets sets Wallets field to given value.

### HasWallets

`func (o *Asset) HasWallets() bool`

HasWallets returns a boolean if a field has been set.

### GetAttributes

`func (o *Asset) GetAttributes() []AssetTrait`

GetAttributes returns the Attributes field if non-nil, zero value otherwise.

### GetAttributesOk

`func (o *Asset) GetAttributesOk() (*[]AssetTrait, bool)`

GetAttributesOk returns a tuple with the Attributes field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetAttributes

`func (o *Asset) SetAttributes(v []AssetTrait)`

SetAttributes sets Attributes field to given value.

### HasAttributes

`func (o *Asset) HasAttributes() bool`

HasAttributes returns a boolean if a field has been set.

### GetMintDate

`func (o *Asset) GetMintDate() int64`

GetMintDate returns the MintDate field if non-nil, zero value otherwise.

### GetMintDateOk

`func (o *Asset) GetMintDateOk() (*int64, bool)`

GetMintDateOk returns a tuple with the MintDate field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMintDate

`func (o *Asset) SetMintDate(v int64)`

SetMintDate sets MintDate field to given value.

### HasMintDate

`func (o *Asset) HasMintDate() bool`

HasMintDate returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


