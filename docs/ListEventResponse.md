# ListEventResponse

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Data** | Pointer to [**[]NFTEvent**](NFTEvent.md) |  | [optional] 
**Meta** | Pointer to [**Meta**](Meta.md) |  | [optional] 

## Methods

### NewListEventResponse

`func NewListEventResponse() *ListEventResponse`

NewListEventResponse instantiates a new ListEventResponse object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewListEventResponseWithDefaults

`func NewListEventResponseWithDefaults() *ListEventResponse`

NewListEventResponseWithDefaults instantiates a new ListEventResponse object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetData

`func (o *ListEventResponse) GetData() []NFTEvent`

GetData returns the Data field if non-nil, zero value otherwise.

### GetDataOk

`func (o *ListEventResponse) GetDataOk() (*[]NFTEvent, bool)`

GetDataOk returns a tuple with the Data field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetData

`func (o *ListEventResponse) SetData(v []NFTEvent)`

SetData sets Data field to given value.

### HasData

`func (o *ListEventResponse) HasData() bool`

HasData returns a boolean if a field has been set.

### GetMeta

`func (o *ListEventResponse) GetMeta() Meta`

GetMeta returns the Meta field if non-nil, zero value otherwise.

### GetMetaOk

`func (o *ListEventResponse) GetMetaOk() (*Meta, bool)`

GetMetaOk returns a tuple with the Meta field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetMeta

`func (o *ListEventResponse) SetMeta(v Meta)`

SetMeta sets Meta field to given value.

### HasMeta

`func (o *ListEventResponse) HasMeta() bool`

HasMeta returns a boolean if a field has been set.


[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


