# TxPage

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**Total** | Pointer to **int32** | Number of items in txs | [optional] 
**Items** | Pointer to [**[]Tx**](Tx.md) |  | [optional] 
**Continuation** | Pointer to **NullableString** | Token to get the next page | [optional] 

## Methods

### NewTxPage

`func NewTxPage() *TxPage`

NewTxPage instantiates a new TxPage object
This constructor will assign default values to properties that have it defined,
and makes sure properties required by API are set, but the set of arguments
will change when the set of required properties is changed

### NewTxPageWithDefaults

`func NewTxPageWithDefaults() *TxPage`

NewTxPageWithDefaults instantiates a new TxPage object
This constructor will only assign default values to properties that have it defined,
but it doesn't guarantee that properties required by API are set

### GetTotal

`func (o *TxPage) GetTotal() int32`

GetTotal returns the Total field if non-nil, zero value otherwise.

### GetTotalOk

`func (o *TxPage) GetTotalOk() (*int32, bool)`

GetTotalOk returns a tuple with the Total field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetTotal

`func (o *TxPage) SetTotal(v int32)`

SetTotal sets Total field to given value.

### HasTotal

`func (o *TxPage) HasTotal() bool`

HasTotal returns a boolean if a field has been set.

### GetItems

`func (o *TxPage) GetItems() []Tx`

GetItems returns the Items field if non-nil, zero value otherwise.

### GetItemsOk

`func (o *TxPage) GetItemsOk() (*[]Tx, bool)`

GetItemsOk returns a tuple with the Items field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetItems

`func (o *TxPage) SetItems(v []Tx)`

SetItems sets Items field to given value.

### HasItems

`func (o *TxPage) HasItems() bool`

HasItems returns a boolean if a field has been set.

### GetContinuation

`func (o *TxPage) GetContinuation() string`

GetContinuation returns the Continuation field if non-nil, zero value otherwise.

### GetContinuationOk

`func (o *TxPage) GetContinuationOk() (*string, bool)`

GetContinuationOk returns a tuple with the Continuation field if it's non-nil, zero value otherwise
and a boolean to check if the value has been set.

### SetContinuation

`func (o *TxPage) SetContinuation(v string)`

SetContinuation sets Continuation field to given value.

### HasContinuation

`func (o *TxPage) HasContinuation() bool`

HasContinuation returns a boolean if a field has been set.

### SetContinuationNil

`func (o *TxPage) SetContinuationNil(b bool)`

 SetContinuationNil sets the value for Continuation to be an explicit nil

### UnsetContinuation
`func (o *TxPage) UnsetContinuation()`

UnsetContinuation ensures that no value is present for Continuation, not even an explicit nil

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


