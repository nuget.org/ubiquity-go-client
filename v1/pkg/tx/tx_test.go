package ubiquitytx

import (
	"context"
	"fmt"
	"math/big"
	"net/http"
	"testing"

	testifyAssert "github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/mock"
	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
	"gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/tx/mocks"
)

func TestBitcoinTransaction_validate(t *testing.T) {
	assert := testifyAssert.New(t)

	testCases := []BitcoinTransaction{
		{
			From:       []TxInput{{Source: "abc", Index: 0}},
			To:         []TxOutput{{Destination: "def", Amount: 12345}},
			PrivateKey: "<private key>",
		},
		{
			Network:    "testnet",
			To:         []TxOutput{{Destination: "def", Amount: 12345}},
			PrivateKey: "<private key>",
		},
		{
			Network:    "testnet",
			From:       []TxInput{{Source: "abc", Index: 0}},
			PrivateKey: "<private key>",
		},
		{
			Network: "testnet",
			From:    []TxInput{{Source: "abc", Index: 0}},
			To:      []TxOutput{{Destination: "def", Amount: 12345}},
		},
		{
			Network:    "testnet",
			From:       []TxInput{{}},
			To:         []TxOutput{{Destination: "def", Amount: 12345}},
			PrivateKey: "<private key>",
		},
		{
			Network:    "testnet",
			From:       []TxInput{{Source: "abc", Index: 0}},
			To:         []TxOutput{{}},
			PrivateKey: "<private key>",
		},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%v", tc), func(t *testing.T) {
			assert.Error(tc.validate())
		})
	}
}

func TestUbiquityTransactionService_SendBTCTestnet(t *testing.T) {
	assert := testifyAssert.New(t)

	txAPIMock := new(mocks.TransactionsAPI)

	ctx := context.Background()
	network := "testnet"
	txSendRequest := ubiquity.ApiTxSendRequest{ApiService: txAPIMock}
	txAPIMock.On("TxSend", ctx, "bitcoin", network).Return(txSendRequest)
	newTxID := "<new tx id>"
	txAPIMock.On("TxSendExecute", mock.AnythingOfType("ubiquity.ApiTxSendRequest")).
		Return(ubiquity.TxReceipt{Id: newTxID}, &http.Response{StatusCode: 200, Status: "200 OK"}, nil)

	txService := NewService(txAPIMock)

	r, err := txService.SendBTC(ctx, &BitcoinTransaction{
		Network: network,
		From:    []TxInput{{Source: "3c7be4102cefc284ef36237a09a3facaa16cec0ffa7eab36302dbf1294734e09", Index: 1}},
		To: []TxOutput{
			{Destination: "mqbGHFBYtsHo5rWoPdCgjoJcVPig96UWv8", Amount: 1000}, // Testnet addresses
			{Destination: "n17f2LuLKddqWj59a4wUc8Ar7iakcPdsEo", Amount: 8000},
		},
		PrivateKey: "P9Z49xp3dXnY5gvbjr2Wc3bDv2U7JovE75qbjJfGJFicek7tFAsX",
	})

	assert.NoError(err)
	assert.Equal(newTxID, r.TxID)
	assert.NotEmpty(r.TxHash)

	txAPIMock.AssertExpectations(t)
}

func TestUbiquityTransactionService_SendBTCMainnet(t *testing.T) {
	assert := testifyAssert.New(t)

	txAPIMock := new(mocks.TransactionsAPI)

	ctx := context.Background()
	network := "mainnet"
	txSendRequest := ubiquity.ApiTxSendRequest{ApiService: txAPIMock}
	txAPIMock.On("TxSend", ctx, "bitcoin", network).Return(txSendRequest)
	newTxID := "<new tx id>"
	txAPIMock.On("TxSendExecute", mock.AnythingOfType("ubiquity.ApiTxSendRequest")).
		Return(ubiquity.TxReceipt{Id: newTxID}, &http.Response{StatusCode: 200, Status: "200 OK"}, nil)

	txService := NewService(txAPIMock)

	r, err := txService.SendBTC(ctx, &BitcoinTransaction{
		Network: network,
		From:    []TxInput{{Source: "782ad4d316fa75262bc70be944ca23a39c4b41e5f83368dc67d176dddef169ba", Index: 1}},
		To: []TxOutput{
			{Destination: "35DjpnyX3MySbHbCN4PYq9qnKbDq8sN4Ka", Amount: 1000}, // Mainnet addresses
			{Destination: "1Kr6QSydW9bFQG1mXiPNNu6WpJGmUa9i1g", Amount: 8000},
		},
		PrivateKey: "P9Z49xp3dXnY5gvbjr2Wc3bDv2U7JovE75qbjJfGJFicek7tFAsX", // We can use same private key
	})

	assert.NoError(err)
	assert.Equal(newTxID, r.TxID)
	assert.NotEmpty(r.TxHash)

	txAPIMock.AssertExpectations(t)
}

func TestEthereumTransaction_validate(t *testing.T) {
	assert := testifyAssert.New(t)

	testCases := []EthereumTransaction{
		{
			PrivateKey: "<private key>",
			To:         "abc",
			Amount:     big.NewInt(12345),
			Nonce:      uint64(1),
		},
		{
			Network: "testnet",
			To:      "abc",
			Amount:  big.NewInt(12345),
			Nonce:   uint64(1),
		},
		{
			Network:    "testnet",
			PrivateKey: "<private key>",
			Amount:     big.NewInt(12345),
			Nonce:      uint64(1),
		},
		{
			Network:    "testnet",
			PrivateKey: "<private key>",
			To:         "abc",
			Nonce:      uint64(1),
		},
	}

	for _, tc := range testCases {
		t.Run(fmt.Sprintf("%v", tc), func(t *testing.T) {
			assert.Error(tc.validate())
		})
	}
}

func TestUbiquityTransactionService_SendETH(t *testing.T) {
	var gasLimit uint64 = 21000

	testCases := []struct {
		name, network string
		estimatedFee  int
		gasLimit      *uint64
		gasPrice      *big.Int
	}{
		{
			name:     "TestnetWithGasLimitAndPrice",
			network:  "ropsten",
			gasLimit: &gasLimit,
			gasPrice: big.NewInt(1000000),
		},
		{
			name:         "TestnetWithoutGasLimit",
			network:      "ropsten",
			gasLimit:     &gasLimit,
			estimatedFee: 2000,
		},
		{
			name:     "MainnetWithGasLimitAndPrice",
			network:  "mainnet",
			gasLimit: &gasLimit,
			gasPrice: big.NewInt(1000000),
		},
		{
			name:         "UnsupportedNetworkWithCleanEstimatedFee",
			network:      "unsupported_network",
			estimatedFee: 12345,
		},
	}

	for _, tc := range testCases {
		t.Run(tc.name, func(t *testing.T) {
			testSendETH(t, tc.network, tc.gasLimit, tc.gasPrice, tc.estimatedFee)
		})
	}
}

func testSendETH(t *testing.T, network string, gasLimit *uint64, gasPrice *big.Int, estimatedFee int) {
	assert := testifyAssert.New(t)

	txAPIMock := new(mocks.TransactionsAPI)

	ctx := context.Background()
	newTxID := "<new tx id>"

	supportedChains := map[string]bool{"mainnet": true, "ropsten": true}
	expectUnsupportedNetworkErr := !supportedChains[network]

	if !expectUnsupportedNetworkErr {
		txSendRequest := ubiquity.ApiTxSendRequest{ApiService: txAPIMock}
		txAPIMock.On("TxSend", ctx, "ethereum", network).Return(txSendRequest)
		txAPIMock.On("TxSendExecute", mock.AnythingOfType("ubiquity.ApiTxSendRequest")).
			Return(ubiquity.TxReceipt{Id: newTxID}, &http.Response{StatusCode: 200, Status: "200 OK"}, nil)
	}

	txService := NewService(txAPIMock)

	var nonNilGasLimit uint64 = 0
	if gasLimit != nil {
		nonNilGasLimit = *gasLimit
	}

	if gasPrice == nil {
		lastBlock := int32(10000)
		estimatedFees := &ubiquity.FeeEstimateEstimatedFees{
			Slow:   estimatedFee,
			Medium: estimatedFee,
			Fast:   estimatedFee,
		}
		feeResp := ubiquity.FeeEstimate{
			MostRecentBlock: &lastBlock,
			EstimatedFees:   estimatedFees,
		}

		estimateFeeRequest := ubiquity.ApiFeeEstimateRequest{ApiService: txAPIMock}
		txAPIMock.On("FeeEstimate", ctx, "ethereum", network).Return(estimateFeeRequest)
		txAPIMock.On("FeeEstimateExecute", mock.AnythingOfType("ubiquity.ApiFeeEstimateRequest")).
			Return(
				feeResp,
				&http.Response{StatusCode: 200, Status: "200 OK"},
				nil,
			)
	}

	r, err := txService.SendETH(ctx, &EthereumTransaction{
		Network:    network,
		PrivateKey: "56f8b9c2469e3d1fec30fa7e0623ca2c48e861bb9384977178c6a72bdde38cd0",
		To:         "0x47128c68cF64a0aff7b436909e1Bd9A46168c93C",
		Amount:     big.NewInt(1000000000),
		GasLimit:   nonNilGasLimit,
		GasPrice:   gasPrice,
		Nonce:      uint64(10),
	})

	if !expectUnsupportedNetworkErr {
		assert.NoError(err)
		assert.Equal(newTxID, r.TxID)
		assert.NotEmpty(r.TxHash)
	} else {
		assert.Error(err)
	}

	txAPIMock.AssertExpectations(t)
}
