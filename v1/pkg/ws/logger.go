package ubiquityws

import (
	"fmt"
	"log"
)

// wsLogger is a simple logger wrapper over log package.
type wsLogger struct {
	showDebug bool
	*log.Logger
}

func (l wsLogger) debug(msg string, args ...interface{}) {
	if l.showDebug {
		l.Println("[DEBUG]", fmt.Sprintf(msg, args...))
	}
}

func (l wsLogger) info(msg string, args ...interface{}) {
	l.Println("[INFO]", fmt.Sprintf(msg, args...))
}

func (l wsLogger) warn(msg string, args ...interface{}) {
	l.Println("[WARN]", fmt.Sprintf(msg, args...))
}

func (l wsLogger) error(msg string, args ...interface{}) {
	l.Println("[ERROR]", fmt.Sprintf(msg, args...))
}
