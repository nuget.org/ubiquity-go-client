package ubiquityws

import (
	"encoding/json"

	ubiquity "gitlab.com/Blockdaemon/ubiquity/ubiquity-go-client/v1/pkg/client"
)

type wsRequest struct {
	ID     int64                  `json:"id"`
	Method string                 `json:"method"`
	Params map[string]interface{} `json:"params"`
}

type BlockIdentifier struct {
	*ubiquity.BlockIdentifier
	Revert bool
}

type Block struct {
	*ubiquity.Block
	Revert bool
}

type Tx struct {
	*ubiquity.Tx
	Revert bool
}

func newSubscribeRequest(reqID int64, channel string, details map[string]interface{}) *wsRequest {
	return &wsRequest{
		ID:     reqID,
		Method: "ubiquity.subscribe",
		Params: map[string]interface{}{
			"channel": channel,
			"detail":  details,
		},
	}
}

func newUnsubscribeRequest(reqID int64, channel string, subID int64) *wsRequest {
	return &wsRequest{
		ID:     reqID,
		Method: "ubiquity.unsubscribe",
		Params: map[string]interface{}{
			"channel": channel,
			"subID":   subID,
		},
	}
}

type wsNotificationParams struct {
	Items []*wsNotificationParamsItem `json:"items"`
}

type wsNotificationParamsItem struct {
	SubID   int64           `json:"subID"`
	Channel string          `json:"channel"`
	Revert  bool            `json:"revert"`
	Content json.RawMessage `json:"content"`
}
